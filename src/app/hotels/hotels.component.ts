import { Component, OnInit } from '@angular/core';

import {Response} from '@angular/http'


import { HotelDataService } from '../service/hotel.service';

 

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {
  hotellist: any;

  feedback:any[] =[];


  AvailableHotels:any [] = [
                            {place:"Bengalore",image : "../assets/images/bengaluru_mdpi.png"},
                            {place:"Hyderabad",image : "../assets/images/hyderabad_mdpi.png"},
                            {place:"Chennai",image : "../assets/images/chennai_mdpi.png"},
                            {place:"Delhi",image : "../assets/images/delhi_mdpi.png"},
                            {place:"Goa",image : "../assets/images/Goa_mdpi.png"},
                            {place:"Bengalore",image : "../assets/images/bengaluru_mdpi.png"},
                            {place:"Hyderabad",image : "../assets/images/hyderabad_mdpi.png"},
                            {place:"Chennai",image : "../assets/images/chennai_mdpi.png"},
                            {place:"Delhi",image : "../assets/images/delhi_mdpi.png"},
                            {place:"Goa",image : "../assets/images/Goa_mdpi.png"},
                            {place:"Bengalore",image : "../assets/images/bengaluru_mdpi.png"},
                            {place:"Hyderabad",image : "../assets/images/hyderabad_mdpi.png"},
                            {place:"Chennai",image : "../assets/images/chennai_mdpi.png"},
                            {place:"Delhi",image : "../assets/images/delhi_mdpi.png"},
                            {place:"Goa",image : "../assets/images/Goa_mdpi.png"},
                            {place:"Bengalore",image : "../assets/images/bengaluru_mdpi.png"},
                            {place:"Bengalore",image : "../assets/images/bengaluru_mdpi.png"},
                            {place:"Hyderabad",image : "../assets/images/hyderabad_mdpi.png"},
                            {place:"Chennai",image : "../assets/images/chennai_mdpi.png"},
                            {place:"Delhi",image : "../assets/images/delhi_mdpi.png"},
                            {place:"Goa",image : "../assets/images/Goa_mdpi.png"},
                    ]
  constructor(private hoteldataservice : HotelDataService) { }

  ngOnInit() {

  //  this.hoteldataservice.getHotelData()
  

      this.hoteldataservice.getHotelData()
    .subscribe(
      (feedback: any[])=>{
      this.feedback = feedback
          console.log("feedback data",feedback);
      },
      (error)=> { console.log(error)
      })
    }
    
      gathotelListData(hotellist: string){
          this.hoteldataservice.getHotelsList(this.hotellist)
          .subscribe(
            (hotellistdata: any[])=>{
            this.feedback = hotellistdata
                console.log("getHotelsList data",hotellistdata);
            },
            (error)=> { console.log(error)
            })
          }
      
    }
