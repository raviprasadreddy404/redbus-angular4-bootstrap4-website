import { Component, OnInit } from '@angular/core';


import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-headers',
  templateUrl: './headers.component.html',
  styleUrls: ['./headers.component.css']
})
export class HeadersComponent implements OnInit {

  siteNav:any;
  
  
    constructor(private activatedRoute: ActivatedRoute){
      this.siteNav ={
          siteName : "A4-B4",
          Logo : "../assets/images/redbuslogo.jpg",
        //  Logo : "https://www.redbus.in/i/cb3451535afb30d5062bffcf31a5b123.png",
          LeftSideNav:["HOME","ABOUT","CONTACTS","HOTELS"],
          RightSideNav :["SIgnUp","SignIn"]
  
  
      }
    }
    ngOnInit() {

      this.activatedRoute.params.subscribe(params => {
        if (params["modal"] == 'true') {
            // Launch Modal here
        }
    });

    }
    

}
