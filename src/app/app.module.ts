import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';



import { RouterModule, Routes } from '@angular/router';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { HttpModule }    from '@angular/http';



import { AppComponent } from './app.component';
import { HeadersComponent } from './headers/headers.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { HotelsComponent } from './hotels/hotels.component';
import { HotelDataService } from './service/hotel.service';




const appRoutes: Routes = [
  { path: 'HOME',  component: HomeComponent },
  { path: 'ABOUT',  component: AboutComponent },
  { path: 'CONTACTS',  component: ContactsComponent },
  { path: 'HOTELS',  component: HotelsComponent },
  
  { path: 'SIgnUp',  component: SignUpComponent },
  { path: 'SignIn',  component: SignInComponent },
  { path: '',  component: HomeComponent }
  
  
  
  
 ];


@NgModule({
  declarations: [
    AppComponent,
    HeadersComponent,
    HomeComponent,
    AboutComponent,
    ContactsComponent,
    SignUpComponent,
    SignInComponent,
    HotelsComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [HotelDataService  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
