
import { Injectable } from '@angular/core';
import { Http , Response,Headers, RequestOptions }       from '@angular/http';

import 'rxjs/add/operator/map';


import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class HotelDataService{
    
    constructor(private http: Http) {}
    
    
    getHotelData(){

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });


        return this.http.get(`http://localhost:3000/hotels`, options)
        .map((res:Response)=> {
            const data = res.json();
            console.log("data added" + res.json());
            return data;
        })
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

    }



    getHotelsList(data : string){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return  this.http.get("http://localhost:3000/hotelsList/${data}", options)
         .map((res:Response)=> {
            const HotelListdata = res.json();
            console.log("hotellist  added" + res.json());
            return HotelListdata;
        })
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

    }



    }

